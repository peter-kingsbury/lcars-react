# LCARS React

A modern React library with LCARS styling.

## Design Styling

* Reference image:
    * ![alt](https://i.pinimg.com/originals/07/2a/7d/072a7d28069383457632bad4bf2395b6.png)

## References

* LCARS - https://joernweissenborn.github.io/lcars/
* Demo - https://github.com/joernweissenborn/lcars/blob/gh-pages/index.html
* React integration - https://github.com/joernweissenborn/lcars/issues/71
* https://startrekdesignproject.com/
* Media Files - https://www.stdimension.org/int/index.htm
* LCARS SDK - https://github.com/Aricwithana/LCARS-SDK

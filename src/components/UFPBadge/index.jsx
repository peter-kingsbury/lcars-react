import React, { Component } from 'react';

import badge from '../../assets/images/UFP2260s.png';

export default class UFPBadge extends Component {
  render() {
    return (
      <div>
        <img src={badge} alt="United Federation of Planets" />
      </div>
    );
  }
}

import React, { Component } from 'react';
import 'LCARS/lcars/css/lcars.devel.css';
import './style.css';

import UFPBadge from '../UFPBadge';

export default class App extends Component {
  render() {
    return (
      <div>
        <div className="lcars-app-container">
          <div id="header" className="lcars-row header">
            <div className="lcars-elbow left-bottom lcars-tan-bg" />

            <div className="lcars-bar horizontal">
              <div className="lcars-title right">LCARS</div>
            </div>

            <div className="lcars-bar horizontal right-end decorated" />

            <div id="left-menu" className="lcars-column start-space lcars-u-1">
              <div className="lcars-bar lcars-u-1" />
            </div>

            <div id="footer" className="lcars-row ">
              <div className="lcars-elbow left-top lcars-tan-bg" />
              <div className="lcars-bar horizontal both-divider bottom">
                <div>status line</div>
              </div>
              <div className="lcars-bar horizontal right-end left-divider bottom" />
            </div>
          </div>

          <div id="container">
            <div className="lcars-column lcars-u-5">
              <UFPBadge />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
